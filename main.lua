local socket = require("socket")
level = "menu"
active_box = "username"
user = ""
ip = "127.0.0.1"
text = ""
position = "0"
time = 0
xy = {0,0,0,0}

function love.load()
  udp = socket.udp();
  udp:settimeout(0);
  map = love.graphics.newImage("MAP.png")
  brain1 = love.graphics.newImage("Brainright.png")
  brain2 = love.graphics.newImage("Brainleft.png")
end
function love.quit()
  udp:sendto("d",ip,29310)
end

--Get values from server
function love.update(dt)
  if level == "game" then
    time = time + dt
    if time > 1 then
      time = time - 1
      udp:sendto("g",ip,29310)
    end
    dg = udp:receive()
    if dg then
      print(dg)
      if string.sub(dg,1,1) == "u" then
        dg = string.sub(dg,2)
        for i=1,4 do
          if string.sub(dg,1,1) == "+" then
            xy[i] = tonumber(string.sub(dg,2,7))
          else
            xy[i] = tonumber(string.sub(dg,1,7))
          end
          dg = string.sub(dg,8)
        end
      elseif string.sub(dg,1,1) == "q" then
        position = string.sub(dg,2)
      end
    end
  end
end

--Draw level + characters
function love.draw()
  if level == "menu" then
    --love.graphics.setBackgroundColor(255,255,255)
    love.graphics.print( "Username: " .. user, 400, 300)
    love.graphics.print( "Server IP: " .. ip, 400, 400)
  else
    love.graphics.draw(map)
    love.graphics.print( "You are in spot " .. position .." in the queue",50,50)
    love.graphics.draw(brain1,xy[1]*10,xy[2]*10,0,0.1,0.1)
    love.graphics.draw(brain1,xy[3]*10,xy[4]*10,0,0.1,0.1)
  end
end
function love.textinput(t)
  if active_box == "username" and level == "menu" then 
    user = user .. t
  elseif level == "menu" then
    ip = ip .. t
  end
end
function love.keypressed(key)
  if key == "backspace" then
    if active_box == "username" then
      user = string.sub(user, 1, string.len(user)-1)
    else
      ip = string.sub(ip, 1, string.len(ip)-1)
    end
  end
  if key == "return" and level== "menu" then
    level = "game"
    udp:sendto("c" ..string.char(0)..user,ip,29310)
  end
end
function love.mousepressed(x, y, button)
  if button == "l" and level == "menu" then
     if y < 350 then
       active_box = "username"
     else 
       active_box = "ip"
     end
  end
end
